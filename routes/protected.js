const express = require('express');
const router = express.Router();

router.use((req, res, next) => {
  if (!req.session.user) {
    return res.status(401).json({ message: 'Unauthorized' });
  }
  next();
});

router.get('/', (req, res) => {
  res.json({ message: 'Welcome to the protected route!' });
});

module.exports = router;