const express = require('express');
const router = express.Router();
const User = require('../models/user');
const validate = require('../middleware/validate');

router.post('/register', validate, async (req, res) => {
  try {
    const user = new User(req.body);
    await user.save();
    res.status(201).json({ message: 'User created successfully' });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
});

router.post('/login', async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (!user) {
      return res.status(401).json({ message: 'Invalid username or password' });
    }
    if (user.password!== req.body.password) {
      return res.status(401).json({ message: 'Invalid username or password' });
    }
    req.session.user = user;
    res.json({ message: 'Logged in successfully' });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
});

router.get('/logout', (req, res) => {
  req.session.destroy();
  res.json({ message: 'Logged out successfully' });
});

module.exports = router;