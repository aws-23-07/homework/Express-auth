const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret:'secret', resave: false, saveUninitialized: true }));

const authRoutes = require('./routes/auth');
app.use('/auth', authRoutes);

const protectedRoute = require('./routes/protected');
app.use('/protected', protectedRoute);

app.listen(3000, () => console.log('Server started on port 3000'));